import java.util.Objects;

import static java.lang.Long.parseLong;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction>, Cloneable {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {
        Lfraction test1 = valueOf("-2/4");
        Lfraction test2 = valueOf("-2/4/6");;
        Lfraction test3 = valueOf("-2/4/");
        
        Lfraction test4 = valueOf("-2/x");
        ;
//        System.out.println(String.format("1) %1$s;\n2) %2$s;\n3) %3$s;\n4) %4$s\n",
//                test1,
//                test2,
//                test3,
//                test4));
//        System.out.println(test1.equals(test2));
//        System.out.println(test2.hashCode());

//        System.out.println(test1 + " - " + test2 + " = " + test1.minus(test2));
//        System.out.println(test1 + " * " + test2 + " = " + test1.times(test2));
//        System.out.println(test1 + " / " + test2 + " = " + test1.divideBy(test2));
//        System.out.println(test2.inverse());
//        System.out.println(test2.opposite());
//
//        System.out.println(test1 + " & " + test2 + " => " + (test1.compareTo(test2)));
//        System.out.println(test2 + " & " + test1 + " => " + (test2.compareTo(test1)));
//        System.out.println(test4 + " & " + test1 + " => " + (test4.compareTo(test1)));
//
//        System.out.println(test3.integerPart());
//        System.out.println(test3.fractionPart());

//        System.out.println(toLfraction(Math.PI, 7));
//        System.out.println(toLfraction(6.25, 5));

    }


    private long numerator;
    private long denominator;

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b < 0) {
            a *= -1;
            b *= -1;
        }

        if (a % b == 0) {
            numerator = a / b;
            denominator = 1;
            return;
        }

        boolean negative = false;
        if (a < 0) {
            negative = true;
            a = -a;
        }

        long i = getGCD(a, b);
        numerator = negative ? (-a / i) : (a / i);
        denominator = b / i;
    }


    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return String.format("%1$s/%2$s", numerator, denominator);
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        if (!(m instanceof Lfraction))
            throw new RuntimeException(String.format("The objects %1$s and %2$s can no be compared", this, m));
        return compareTo((Lfraction) m) == 0;
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(numerator, denominator);
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        if (denominator == m.getDenominator()) {
            return new Lfraction(numerator + m.getNumerator(), denominator);
        }

        return new Lfraction(
                ((numerator * m.getDenominator() + m.getNumerator() * denominator)),
                denominator * m.getDenominator());
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        return new Lfraction(numerator * m.getNumerator(), denominator * m.getDenominator());
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (numerator == 0) throw new RuntimeException("Cannot inverse the fraction: denominator cannot be 0");
        return new Lfraction(denominator, numerator);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(-numerator, denominator);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
//        if (denominator == m.getDenominator()) {
//            return new Lfraction(numerator - m.getNumerator(), denominator);
//        }
//        return new Lfraction(
//                (numerator * m.getDenominator() - m.getNumerator() * denominator),
//                denominator * m.getDenominator());
        return plus(m.opposite());
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
//        if (numerator == 0) throw new RuntimeException("Cannot inverse the fraction: denominator cannot be 0");
//        return new Lfraction(numerator * m.getDenominator(), denominator * m.getNumerator());
        return times(m.inverse());
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        long indexA = 1;
        long indexB = 1;
        if (denominator != m.getDenominator()) {
            indexA = m.getDenominator();
            indexB = denominator;
        }
        long cacheA = numerator * indexA;
        long cacheB = m.getNumerator() * indexB;
        return Long.compare(cacheA, cacheB);

    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        if (denominator < 0) throw new CloneNotSupportedException("Object cannot be cloned");
        return new Lfraction(numerator, denominator);
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return numerator / denominator;
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        if (Math.abs(numerator) > denominator) {
            return new Lfraction(numerator - denominator * integerPart(), denominator);
        }
        return this;
    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return ((double) numerator / (double) denominator);
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        return new Lfraction(Math.round(f * d), d);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        if (s.trim() == "") throw new RuntimeException(String.format("The string '%1$s' is empty", s));
        String[] numbers = s.trim().split("");
        if (numbers[numbers.length - 1].equals("/"))
            throw new RuntimeException(String.format("The string '%1$s' is not a fraction", s));
        numbers = s.trim().split("/");
        if (numbers.length > 2) throw new RuntimeException(String.format("The string '%1$s' is not a fraction", s));
        long a = 0;
        String t = "";
        try {
            for (String number : numbers) {
                t = number;
                a = parseLong(number);
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format
                    ("The string '%1$s' is not a fraction. %2$s cannot be recognised as a Long", s, t));
        }
        return new Lfraction(parseLong(numbers[0]), parseLong(numbers[1]));
    }

    private long getGCD(long a, long b) {
//        for (long i =  min(a, b); i > 0; i--) {
//            if (a % i == 0 && b % i == 0) return i;
//        }
//        return 1;

        //  Code from GeeksFromGeeks.org
        if (b == 0)
            return a;
        return getGCD(b, a % b);

    }
}

